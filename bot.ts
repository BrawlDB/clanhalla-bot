import * as Knex from 'knex';
import * as Discord from 'discord.js';
import { DMChannel, GroupDMChannel, Message, RichEmbed, TextChannel } from 'discord.js';
import * as fs from 'fs';
import { BrawlhallaApiModule, ClaimApiModule, DatabaseModule, ServerInstance, ServerModule } from './modules';
import { ConfigType } from './config.type';
import { Command, CommandArgument } from './commands/command';
import { Module } from './modules/module';
import { ClanInstance, DuelInstance, HelpInstance, PingInstance, PrefixInstance, StatsInstance } from './commands';
import { isReactiveEmbed, ReactiveRichEmbed } from './utils/ReactiveEmbed';

export type ModuleType = {
  ServerModule: ServerModule,
  ClaimApiModule: ClaimApiModule,
  BrawlhallaApiModule: BrawlhallaApiModule
}
export type ContextType = {
  server: ServerInstance
}

class Bot {
  private config: ConfigType;
  private db!: Knex;

  private commandMap!: Map<string, Command>;

  private modules!: Module[];
  private moduleObject!: ModuleType;
  private serverModule!: ServerModule;

  private client!: Discord.Client;

  constructor() {
    console.info('Hello World!\nBot is about to launch.');
    this.config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
    this.init();
  }

  private async init(): Promise<void> {
    console.info('Initializing database...');
    const db = new DatabaseModule();
    await db.init(this.config);
    this.db = await db.getDb();
    console.info('Initializing modules...');
    await this.initModules();
    console.info('Initializing commands...');
    await this.initCommands();
    console.info('Initializing client...');
    await this.initClient();
    console.info('Done!\nEverything should be online.');
  }

  private async initModules(): Promise<void> {
    // tslint:distable-next-line:no-any
    const moduleList: any[] = [ ServerModule, BrawlhallaApiModule, ClaimApiModule ];
    this.modules = [];
    const moduleObject: any = {};
    await Promise.all(moduleList.map(async module => {
      const instance = new module();
      await instance.init(this.config, this.db);
      this.modules.push(instance);
      moduleObject[ instance.name ] = instance;
    }));
    this.moduleObject = moduleObject;

    this.serverModule = this.moduleObject[ 'ServerModule' ] as ServerModule;
  }

  private async initCommands(): Promise<void> {
    const commandList: Command[] = [ PingInstance, PrefixInstance, ClanInstance, StatsInstance, DuelInstance ];
    this.commandMap = new Map();

    commandList.forEach(command => {
      command.init(this.moduleObject);
      if (this.commandMap.has(command.name)) {
        console.warn(`Command ${command.name} can't be registed, as its name is already used!`);
      } else {
        this.commandMap.set(command.name, command);
      }
      command.alias.forEach(alias => {
        if (this.commandMap.has(alias)) {
          console.warn(`Alias ${alias} can't be set for command ${command.name}, as it already exists!`);
        } else {
          this.commandMap.set(alias, command);
        }
      });
    });

    HelpInstance.initHelp(commandList);
    this.commandMap.set('help', HelpInstance);

  }

  private async initClient(): Promise<void> {
    const client = new Discord.Client();
    await client.login(this.config.discordToken);
    this.client = client;

    client.on('message', message => this.onMessage(message));
  }

  private async onMessage(message: Discord.Message): Promise<void> {
    const currentServer: ServerInstance = await this.serverModule.getConfig(message.guild);

    // Ignore messages which do not start with the correct prefix
    if (!message.content.startsWith(currentServer.prefix)) {
      return;
    }
    const [ commandName, ...params ] = message.content.substring(currentServer.prefix.length).split(' ');

    if (!this.commandMap.has(commandName)) {
      console.debug('Ignoring message: Not a command');
      // Not a command we support
      return;
    }
    const command = this.commandMap.get(commandName)!;
    const context: ContextType = {
      server: currentServer,
    };

    const replacedParams = command.arguments.map((c, i) => {
      if (i < params.length) {
        return params[ i ];
      } else if (c.default !== undefined) {
        if (c.default === 'CURRENT_USER') {
          return `<@${message.author.id}>`;
        }
        return c.default;
      }
    });
    let parsedArguments: false | any[];
    let error: string = '';
    try {
      parsedArguments = await this.parseArguments(replacedParams, command.arguments);
    } catch (e) {
      error = e;
      parsedArguments = false;
    }
    if (parsedArguments === false) {
      if (error !== '') {
        message.channel.send(error);
      } else {
        this.sendUsageCommand(message.channel, command, currentServer, 'Invalid command!\nTry the following:');
      }
      return;
    }
    console.debug(`Executing command ${command.name} for user ${message.author.username}`);

    const result = await command.execute(context, message, ...parsedArguments);
    if (Array.isArray(result)) {
      result.forEach(v => {
        this.sendMessage(message, v);
      });
    } else {
      this.sendMessage(message, result);
    }
  }

  private sendMessage(message: Message, value: string | RichEmbed | ReactiveRichEmbed) {
    if (isReactiveEmbed(value)) {
      value.sendAndManage(message.channel);
    } else {
      message.channel.send(value);
    }
  }

  private sendUsageCommand(channel: TextChannel | DMChannel | GroupDMChannel,
                           command: Command,
                           server: ServerInstance,
                           prefix = '') {
    let message = prefix;
    if (prefix) {
      message += '\n';
    }
    channel.send(message + `Usage: ${server.prefix}${command.usage}`);
  }

  private async parseArguments(values: string[], types: CommandArgument[]): Promise<false | any[]> {
    // Ensuring we have enough values
    // TODO: Better handling of commands with default values
    const firstUndefinedIndex = values.findIndex(v => !v);
    if ((firstUndefinedIndex === -1 ? values.length : firstUndefinedIndex)
      < types.filter(t => (t.required === undefined || t.required === true) && !t.default).length) {
      console.debug('Not enough values!');
      return false;
    }

    const discordIdRegex = /<@!?(\d+)>/;

    types.every((type, i) => {
      const value = values[ i ];
      let isTypeValid = false;
      switch (type.type) {
        case 'string':
          isTypeValid = true;
          break;
        case 'number':
          isTypeValid = !isNaN(+value);
          break;
        case 'user':
        case 'mention':
        case 'brawlhalla_profile':
          isTypeValid = discordIdRegex.test(value);
          break;
        case 'regex':
          isTypeValid = type.regex!.test(value);
          break
      }
      return isTypeValid;
    });

    return await Promise.all(types.map(async (type, i) => {
      const value = values[ i ];
      if (!value && !type.required) {
        return;
      }
      let parsedValue;
      switch (type.type) {
        case 'string':
          parsedValue = value;
          break;
        case 'number':
          parsedValue = +value;
          break;
        case 'mention':
          parsedValue = this.getUserIdFromMention(value);
          break;
        case 'brawlhalla_profile': {
          const userId = this.getUserIdFromMention(value);
          const claimProfile = await this.moduleObject.ClaimApiModule.getUser(userId);
          if (!claimProfile.bhid) {
            throw 'The mentioned user does not have an attached brawlhalla account!';
          }
          parsedValue = await this.moduleObject.BrawlhallaApiModule.getPlayerStats(claimProfile.bhid);
        }
          break;
        case 'user': {
          const userId = this.getUserIdFromMention(value);
          parsedValue = await this.client.fetchUser(userId);
        }
          break;
        case 'regex':
          parsedValue = type.regex!.exec(value);
          break;
        default:
          throw `Invalid type given!\nDid you forget to support ${type.type}?`;
      }
      return parsedValue;
    }));
  }

  private getUserIdFromMention(mentionText: string): string {
    const discordIdRegex = /<@!?(\d+)>/;
    return discordIdRegex.exec(mentionText)![ 1 ];
  }

}

const instance = new Bot();