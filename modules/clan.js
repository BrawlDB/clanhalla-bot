let _knex;
let discordServerTableName;
let discordClanTableName;

const init = async (knex, config) => {
    _knex = knex;
    discordClanTableName = config.dbClanTable || 'DiscordClan';
    discordServerTableName = config.dbChannelsTable || 'DiscordGuild';

    if (!(await knex.schema.hasTable(discordClanTableName))) {
        console.log(`Table ${discordClanTableName} does not exist! Creating it`);
        await knex.schema.createTable(discordClanTableName, function (table) {
            table.increments();
            table.foreign('server_id').references(`${discordServerTableName}.id`);
            table.string('name');
            table.string('description');
            table.string('region');
            table.timestamp('created_at').defaultTo(knex.fn.now());
        });
        console.log(`Done creating table ${discordClanTableName}`);
    }
};

const getClanByName = async (serverId, clanName) => {
    return await _knex(discordClanTableName)
        .where('server_id', serverId)
        .where('name', clanName)
        .first();
};

const createOrUpdate = async (serverId, clan) => {
    const oldClan = await getClanByName(serverId, clan.name);
    if(oldClan) {
        await _knex(discordClanTableName)
            .where('server_id', serverId)
            .where('name', clan.name)
            .update(clan);
    } else {
        await _knex(discordClanTableName).insert(clan);
    }
};

module.exports = {init, getClanByName, createOrUpdate};
