let _knex;
let discordUsersTableName;

const init = async (knex, config) => {
    _knex = knex;
    discordUsersTableName = config.dbUserTable || 'DiscordUser';

    if (!(await knex.schema.hasTable(discordUsersTableName))) {
        console.log(`Table ${discordUsersTableName} does not exist! Creating it`);
        await knex.schema.createTable(discordUsersTableName, function (table) {
            table.increments();
            table.string('discordId');
            table.string('username');
            table.integer('bhId');
            table.timestamp('created_at').defaultTo(knex.fn.now());
        });
        console.log('Done creating table!');
    }
};

const getUserByDiscordId = async (id) => {
    return await _knex(discordUsersTableName).where('discordId', id).first();
};

const createOrUpdate = async (user) => {
    const oldUser = await getUserByDiscordId(user.discordId);
    if(oldUser) {
        await _knex(discordUsersTableName).where('discordId', user.discordId).update(user);
    } else {
        await _knex(discordUsersTableName).insert(user);
    }
};

module.exports = {init, getUserByDiscordId, createOrUpdate};
