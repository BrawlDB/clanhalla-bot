import * as Knex from 'knex';
import { ConfigType } from '../config.type';

export interface Module {
  name: string;
  init(config: ConfigType, db: Knex): Promise<void>;
}