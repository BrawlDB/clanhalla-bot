import { Module } from './module';
import { ConfigType } from '../config.type';
import * as Knex from 'knex';
import { Guild } from 'discord.js';

export class ServerModule implements Module {
  public defaultPrefix: string = '*';
  public name = 'ServerModule';

  private tableName: string = 'DiscordGuild';
  private db!: Knex;

  public async init(config: ConfigType, db: Knex): Promise<void> {
    if (config.defaultPrefix) {
      this.defaultPrefix = config.defaultPrefix;
    }
    if (config.dbGuildTableName) {
      this.tableName = config.dbGuildTableName;
    }
    this.db = db;
    if (!(await db.schema.hasTable(this.tableName))) {
      await this.buildSchema();
    }
  }

  public async buildSchema() {
    console.log(`Creating table ${this.tableName}`);
    await this.db.schema.createTable(this.tableName, (table) => {
      table.increments();
      table.string('serverId');
      table.string('serverName');
      table.string('prefix').defaultTo(this.defaultPrefix);
      table.timestamp('created_at').defaultTo(this.db.fn.now());
    });
    console.log(`Done creating table ${this.tableName}!`);
  }

  public async createServerConfig(server: Guild): Promise<ServerInstance> {
    const newServer: DatabaseServerInstance = {
      serverId: server.id,
      serverName: server.name,
      prefix: this.defaultPrefix
    };
    await this.db(this.tableName).insert(newServer);

    return this.createServerInstance(newServer);
  }

  public async setPrefix(serverId: string, newPrefix: string):  Promise<void> {
    await this.db(this.tableName).where('serverId', serverId).update({prefix: newPrefix});
  }

  public async getConfig(server: Guild): Promise<ServerInstance> {
    const databaseServer: DatabaseServerInstance = (await this.db(this.tableName).where('serverId', server.id).first()) || await this.createServerConfig(server);
    return this.createServerInstance(databaseServer);
  }

  private createServerInstance(db: DatabaseServerInstance): ServerInstance {
    return {
      ...db,
      setPrefix: (newPrefix: string) => this.setPrefix(db.serverId, newPrefix)
    };
  }
}

export interface ServerInstance {
  serverId: string;
  serverName: string;
  prefix: string;
  setPrefix: (newPrefix: string) => Promise<void>;
}

export interface DatabaseServerInstance {
  serverId: string;
  serverName: string;
  prefix: string;
}
