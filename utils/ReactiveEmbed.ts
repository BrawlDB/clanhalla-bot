import {
  Attachment,
  ColorResolvable,
  DMChannel,
  FileOptions,
  GroupDMChannel,
  Message,
  MessageReaction,
  RichEmbed,
  TextChannel,
  User
} from 'discord.js';

export class ReactiveRichEmbed {
  public author?: { name: string; url?: string; icon_url?: string; };
  public color?: ColorResolvable;
  public description?: string;
  public fields: { name: string; value: string; inline?: boolean; }[];
  public file?: Attachment | string | FileOptions;
  public footer?: { text?: string; icon_url?: string; };
  public image?: { url: string; proxy_url?: string; height?: number; width?: number; };
  public thumbnail?: string;
  public timestamp?: Date;
  public title?: string;
  public url?: string;

  protected botUser?: User;
  protected message?: Message;

  private emojiListeners: { emoji: string, callback: (user: User) => Promise<void> }[];

  constructor() {
    // Default color
    this.color = 'GOLD';
    this.emojiListeners = [];
    this.fields = [];
  }

  public build(): RichEmbed {
    const generatedEmbed = new RichEmbed();
    if (this.author) {
      generatedEmbed.setAuthor(this.author);
    }
    if (this.color) {
      generatedEmbed.setColor(this.color);
    }
    if (this.description) {
      generatedEmbed.setDescription(this.description);
    }
    if (this.footer) {
      generatedEmbed.setFooter(this.footer.text, this.footer.icon_url);
    }
    if (this.thumbnail) {
      generatedEmbed.setThumbnail(this.thumbnail);
    }
    if (this.title) {
      generatedEmbed.setTitle(this.title);
    }
    if (this.url) {
      generatedEmbed.setURL(this.url);
    }

    this.fields.forEach(f => {
      generatedEmbed.addField(f.name, f.value, f.inline);
    });

    return generatedEmbed;
  }

  public setTitle(title: string): this {
    this.title = title;
    return this;
  }

  public setURL(url: string): this {
    this.url = url;
    return this;
  }

  public setDescription(url: string): this {
    this.url = url;
    return this;
  }

  public setColor(color: ColorResolvable): this {
    this.color = color;
    return this;
  }

  public setFooter(text: string, icon?: string): this {
    this.footer = {text, icon_url: icon};
    return this;
  }

  public addField(name: string, value: string, inline: boolean = false): this {
    this.fields.push({name, value, inline});
    return this;
  }

  public replaceField(name: string, value: string) {
    const previousValue = this.fields.find(f => f.name === name);
    if (previousValue) {
      previousValue.value = value;
    } else {
      this.addField(name, value);
    }
    this.rebuild();
  }

  public removeField(name: string) {
    this.fields = this.fields.filter(f => f.name !== name);
  }

  public addBlankField(inline?: boolean): this {
    this.fields.push({name: 'BLANK', value: '', inline});
    return this;
  }

  public async sendAndManage(channel: DMChannel | GroupDMChannel | TextChannel): Promise<void> {
    const sentMessage: Message = await channel.send(this.build()) as Message;
    this.message = sentMessage;
    this.botUser = sentMessage.author;
    this.addReactions();
    this.addReactionListeners();
  }

  public addReactionListener(emoji: string, callback: (user: User) => Promise<void>) {
    this.emojiListeners.push({emoji, callback});
    if (this.message !== undefined && !this.message.reactions.some(r => r.emoji.name === emoji)) {
      this.message.react(emoji);
    }
  }

  public removeReaction(emoji: string) {
    this.emojiListeners = this.emojiListeners.filter(e => e.emoji !== emoji);
    if (this.message) {
      this.message.reactions.filter(e => e.emoji.name === emoji)
        .forEach(r => {
          r.remove();
        });
    }
  }

  public removeReactions() {
    if (this.message) {
      this.message.reactions.forEach(r => {
        r.remove();
      });
    }
    this.emojiListeners = [];
  }


  protected async rebuild(): Promise<void> {
    if (this.message) {
      await this.message.edit('', this.build());
    }
  }

  private async addReactions() {
    for (let i = 0; i < this.emojiListeners.length; i++) {
      const {emoji} = this.emojiListeners[i];
      try {
        await this.message!.react(emoji);
      } catch (e) {
        this.message!.channel.send(e.toString());
      }
    }
  }

  private async addReactionListeners() {
    const reactionEventListener = (reaction: MessageReaction, user: User) => {
      if (user.id !== this.botUser!.id) {
        if (this.emojiListeners.some(e => e.emoji === reaction.emoji.name)) {
          const matchingListener = this.emojiListeners.find(e => e.emoji === reaction.emoji.name);
          matchingListener!.callback(user).then(() => {
            this.rebuild()
          });
        }
        reaction.remove(user);
      }
      return false;
    };

    // TODO: Clear memory after few mins
    this.message!.awaitReactions(reactionEventListener);
  }

}

export function isReactiveEmbed(value: any): value is ReactiveRichEmbed {
  return value instanceof ReactiveRichEmbed;
}
