import {Command, CommandArgument} from './command';
import {ContextType, ModuleType} from '../bot';
import {Message, User} from 'discord.js';
import {ReactiveRichEmbed} from '../utils/ReactiveEmbed';
import {ILegendStats, IPlayerStats, IWeaponStats} from 'brawlhalla-api-ts-interfaces';
import {BrawlhallaApiModule, ClaimApiModule} from '../modules';

export class DuelCommand implements Command {
  public name: string = 'Duel';
  public description: string = 'Challenge another user to a duel!';
  public alias: string[] = ['duel', 'battle'];

  private BrawlhallaApiModule!: BrawlhallaApiModule;
  private ClaimApiModule!: ClaimApiModule;

  public init({BrawlhallaApiModule, ClaimApiModule}: ModuleType) {
    this.BrawlhallaApiModule = BrawlhallaApiModule;
    this.ClaimApiModule = ClaimApiModule;

  }

  public arguments: CommandArgument[] = [
    {
      type: 'user',
      required: true
    },
    {
      type: 'user',
      required: true,
      default: 'CURRENT_USER'
    }];
  public usage: string = 'duel @Mention';

  public async execute(
    context: ContextType,
    message: Message,
    firstPlayer: User,
    secondPlayer: User): Promise<ReactiveRichEmbed> {
    return new DuelEmbed(this.BrawlhallaApiModule, this.ClaimApiModule, firstPlayer, secondPlayer);
  }

}

export const DuelInstance = new DuelCommand();

class DuelEmbed extends ReactiveRichEmbed {

  private BrawlhallaApiModule: BrawlhallaApiModule;
  private ClaimApiModule: ClaimApiModule;
  private firstPlayer: User;
  private secondPlayer: User;
  private firstUserConfirmed: boolean;
  private secondUserConfirmed: boolean;
  private firstPlayerBhid?: number;
  private secondPlayerBhid?: number;

  private firstPlayerStatsBeforeDuel?: IPlayerStats;
  private secondPlayerStatsBeforeDuel?: IPlayerStats;
  private firstPlayerStatsAfterDuel?: IPlayerStats;
  private secondPlayerStatsAfterDuel?: IPlayerStats;

  private confirmIcon = '✅';
  private declineIcon = '❌';
  private duelStartIcon = '⚔';
  private duelEndIcon = '👑';

  constructor(api: BrawlhallaApiModule, claim: ClaimApiModule, first: User, second: User) {
    super();
    this.firstUserConfirmed = false;
    this.secondUserConfirmed = false;
    this.BrawlhallaApiModule = api;
    this.ClaimApiModule = claim;
    this.firstPlayer = first;
    this.secondPlayer = second;
    this.setTitle(
      `${second.username} wants to fight!`
    ).addField('Information', `This is a duel request between ${first} and ${second}.\n` +
      `- Confirm your presence by reacting with ${this.confirmIcon}, or decline with ${this.declineIcon}!\n` +
      `- Start the match with ${this.duelStartIcon}!\n` +
      ` - Play a match against each other\n` +
      ` - When the match is over, react with ${this.duelEndIcon} to get your stats!\n`)
      .addField(first.username, `Status: Grabbing brawlhalla profile...`, true)
      .addField(second.username, `Status: Grabbing brawlhalla profile...`, true)
      .addField('Status', 'Waiting for both confirmations...');

    this.getBhids();
  }

  private async getBhids(): Promise<void> {
    const firstResult = await this.ClaimApiModule.getUser(this.firstPlayer.id);
    if (!firstResult.bhid) {
      this.replaceField(this.firstPlayer.username, 'Status: Needs to claim his Brawlhalla profile!');
    } else {
      this.replaceField(this.firstPlayer.username, 'Status: Player found!');
      this.firstPlayerBhid = firstResult.bhid;
    }
    const secondResult = await this.ClaimApiModule.getUser(this.secondPlayer.id);
    if (!secondResult.bhid) {
      this.replaceField(this.secondPlayer.username, 'Status: Needs to claim his Brawlhalla profile!');
    } else {
      this.replaceField(this.secondPlayer.username, 'Status: Player found!');
      this.secondPlayerBhid = secondResult.bhid;
    }
    if (!firstResult.bhid || !secondResult.bhid) {
      this.replaceField('Status', 'Cancelled! Both players need to claim their profile with Gerard to duel.');
    } else {
      this.initPlayerConfirmations();
    }
  }

  private initPlayerConfirmations(): void {
    this.replaceField(this.firstPlayer.username, 'Status: Waiting for confirmation');
    this.replaceField(this.secondPlayer.username, 'Status: Waiting for confirmation');
    this.addReactionListener(this.confirmIcon, async user => this.confirm(user));
    this.addReactionListener(this.declineIcon, async user => this.decline(user));
  }

  private confirm(user: User): void {
    const isFirstPlayer = user === this.firstPlayer;
    const isSecondPlayer = user === this.secondPlayer;

    if (isFirstPlayer || isSecondPlayer) {
      if (isFirstPlayer) {
        this.firstUserConfirmed = true;
        this.replaceField('Status', `Waiting for ${this.secondPlayer.username}`);
      } else {
        this.secondUserConfirmed = true;
        this.replaceField('Status', `Waiting for ${this.firstPlayer.username}`);
      }
      this.replaceField(user.username, `Confirmed! ${this.confirmIcon}`);
      if (this.firstUserConfirmed && this.secondUserConfirmed) {
        this.removeReaction(this.confirmIcon);
        this.removeReaction(this.declineIcon);
        this.initFightConfirmation();
      }
    }
  }

  private decline(user: User): void {
    const isFirstPlayer = user === this.firstPlayer;
    const isSecondPlayer = user === this.secondPlayer;

    if (isFirstPlayer || isSecondPlayer) {
      this.replaceField(user.username, `Declined! ${this.declineIcon}`);
      this.replaceField('Status', `Fight declined by ${user.username}`);
      this.removeReactions();
    }
  }

  private initFightConfirmation() {
    this.replaceField('Status',
      'Waiting for fight to start!\n' +
      `React with ${this.duelStartIcon} when ready!`);
    this.addReactionListener(this.duelStartIcon, () => this.initMatchStart());
  }

  private async initMatchStart(): Promise<void> {
    this.removeReaction(this.duelStartIcon);
    this.replaceField('Status',
      'Fight started! Waiting for fight to end.\n' +
      `Press the ${this.duelEndIcon} once I react it in 5 minutes!`);
    this.firstPlayerStatsBeforeDuel = await this.BrawlhallaApiModule.getPlayerStats(this.firstPlayerBhid!);
    this.secondPlayerStatsBeforeDuel = await this.BrawlhallaApiModule.getPlayerStats(this.secondPlayerBhid!);
    // In 5 minutes (API cache), add crown
    setTimeout(() => {
      this.addReactionListener(this.duelEndIcon, () => this.endDuel());
    }, 1000 * 60 * 5);
  }

  private async endDuel(): Promise<void> {
    this.removeReaction(this.duelEndIcon);
    this.firstPlayerStatsAfterDuel = await this.BrawlhallaApiModule.getPlayerStats(this.firstPlayerBhid!);
    if (this.firstPlayerStatsBeforeDuel!.games === this.firstPlayerStatsAfterDuel.games) {
      this.waitForDuel();
      return;
    }
    this.secondPlayerStatsAfterDuel = await this.BrawlhallaApiModule.getPlayerStats(this.secondPlayerBhid!);
    if (this.secondPlayerStatsBeforeDuel!.games === this.secondPlayerStatsAfterDuel.games) {
      this.waitForDuel();
      return;
    }
  }

  private async waitForDuel(): Promise<void> {
    setTimeout(() => {
      this.addReactionListener(this.duelEndIcon, () => this.endDuel());
    }, 1000 * 60 * 5);
    this.replaceField('Status',
      'Match not completed (or cached)!\n' +
      `Wait 5 minutes (until the ${this.duelEndIcon} reaction is available) before retrying...`);
  }

  private async abortDuel(reason: string): Promise<void> {
    this.replaceField('Status', `Duel aborted!\n${reason}`);
    this.removeReactions();
  }

  private generateStats(before: IPlayerStats, after: IPlayerStats): void {
    // TODO: Generate the stats, and throw error or build result
  }

  private generatePlayerStats(before: IPlayerStats, after: IPlayerStats): IPlayerStats {
    return {
      brawlhallaID: after.brawlhallaID,
      name: after.name,
      xp: after.xp - before.xp,
      level: after.level,
      xpPercentage: after.xpPercentage - before.xpPercentage,
      games: after.games - before.games,
      wins: after.wins - before.wins,
      damageDealtByBomb: after.damageDealtByBomb - before.damageDealtByBomb,
      damageDealtByMine: after.damageDealtByMine - before.damageDealtByMine,
      damageDealtBySpikeball: after.damageDealtBySpikeball - before.damageDealtBySpikeball,
      damageDealtBySidekick: after.damageDealtBySidekick - before.damageDealtBySidekick,
      snowballHits: after.snowballHits - before.snowballHits,
      koByBomb: after.koByBomb - before.koByBomb,
      koByMine: after.koByMine - before.koByMine,
      koBySpikeball: after.koBySpikeball - before.koBySpikeball,
      koBySidekick: after.koBySidekick - before.koBySidekick,
      koBySnowball: after.koBySnowball - before.koBySnowball,
      clan: after.clan,
      legendStats: after.legendStats.map(legendAfter =>
        this.generateLegendStats(
          before.legendStats.find(legendBefore => legendBefore.legend.id === legendAfter.legend.id)!,
          legendAfter)
      ),
      weaponStats: after.weaponStats.map(weaponAfter =>
        this.generateWeaponStats(
          before.weaponStats.find(weaponBefore => weaponBefore.weapon.id === weaponAfter.weapon.id)!,
          weaponAfter)
      ),
    };
  }

  private generateLegendStats(before: ILegendStats, after: ILegendStats): ILegendStats {
    return {
      legend: after.legend,
      xp: after.xp - before.xp,
      level: after.level,
      xpPercentage: after.xpPercentage - before.xpPercentage,
      damageDealt: after.damageDealt - before.damageDealt,
      damageTaken: after.damageTaken - before.damageTaken,
      kos: after.kos - before.kos,
      falls: after.falls - before.falls,
      suicides: after.suicides - before.suicides,
      teamKos: after.teamKos - before.teamKos,
      matchTime: after.matchTime - before.matchTime,
      games: after.games - before.games,
      wins: after.wins - before.wins,
      defeats: after.defeats - before.defeats,
      damageDealtByUnarmed: after.damageDealtByUnarmed - before.damageDealtByUnarmed,
      damageDealtByThrownItems: after.damageDealtByThrownItems - before.damageDealtByThrownItems,
      damageDealtByFirstWeapon: after.damageDealtByFirstWeapon - before.damageDealtByFirstWeapon,
      damageDealtBySecondWeapon: after.damageDealtBySecondWeapon - before.damageDealtBySecondWeapon,
      damageDealtByGadgets: after.damageDealtByGadgets - before.damageDealtByGadgets,
      koByUnarmed: after.koByUnarmed - before.koByUnarmed,
      koByThrownItems: after.koByThrownItems - before.koByThrownItems,
      koByFirstWeapon: after.koByFirstWeapon - before.koByFirstWeapon,
      koBySecondWeapon: after.koBySecondWeapon - before.koBySecondWeapon,
      koByGadgets: after.koByGadgets - before.koByGadgets,
      timeHeldFirstWeapon: after.timeHeldFirstWeapon - before.timeHeldFirstWeapon,
      timeHeldSecondWeapon: after.timeHeldSecondWeapon - before.timeHeldSecondWeapon,
    }
  }

  private generateWeaponStats(before: IWeaponStats, after: IWeaponStats): IWeaponStats {
    return {
      weapon: after.weapon,
      games: after.games - before.games,
      wins: after.wins - before.wins,
      kos: after.kos - before.kos,
      time: after.time - before.time,
    }
  }

}

interface IMatchStats {

}