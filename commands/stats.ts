import { Command, CommandArgument } from './command';
import { ContextType, ModuleType } from '../bot';
import { Message } from 'discord.js';
import { ReactiveRichEmbed } from '../utils/ReactiveEmbed';
import { ILegendStats, IPlayerStats, IWeaponStats } from 'brawlhalla-api-ts-interfaces';

export class StatsCommand implements Command {
  public name: string = 'Stats';
  public description: string = 'View brawlhalla stats about a user';
  public alias: string[] = [ 'stats' ];
  public arguments: CommandArgument[] = [ {
    type: 'brawlhalla_profile',
    required: true,
    default: 'CURRENT_USER'
  } ];
  public usage: string = 'stats @Mention';

  public init(){}

  public async execute(
    context: ContextType,
    message: Message,
    bhProfile: IPlayerStats): Promise<ReactiveRichEmbed> {
    return this.buildStatsEmbed(bhProfile);
  }

  private buildStatsEmbed(player: IPlayerStats): ReactiveRichEmbed {
    return new PlayerStatsEmbed(player)
      .setTitle(`Stats: ${player.name}`)
      .setColor('GOLD')
      // .setURL(`https://brawldb.com/player/stats/${player.brawlhallaID}`)
      .addField('Information', `Name: [${player.name}](https://brawldb.com/player/stats/${player.brawlhallaID})\nClan: [${player.clan!.name}](https://brawldb.com/clan/info/${player.clan!.id})`, true)
      .addField('Overall', `${player.wins} wins / ${player.games} games (${~~(player.wins * 100 / player.games)}%)`, true)
  }
}

export const StatsInstance = new StatsCommand();


class PlayerStatsEmbed extends ReactiveRichEmbed {
  private weaponsIcon = '⚔';
  private legendsIcon = '🦍';
  private previousElementIcon = '◀';
  private nextElementIcon = '▶';
  private rankedStatsIcon = '👑';

  private stats: IPlayerStats;

  private isCurrentlyOnWeapons = false;
  private currentElementIndex = 0;

  constructor(stats: IPlayerStats) {
    super();
    this.stats = stats;

    this.addReactionListener(this.weaponsIcon, async () => {
      if (!this.isCurrentlyOnWeapons) {
        this.isCurrentlyOnWeapons = true;
        this.currentElementIndex = 0;
      }
    });
    this.addReactionListener(this.legendsIcon, async () => {
      if (this.isCurrentlyOnWeapons) {
        this.isCurrentlyOnWeapons = false;
        this.currentElementIndex = 0;
      }
    });
    this.addReactionListener(this.previousElementIcon, async () => {
      this.currentElementIndex--;
      if (this.currentElementIndex < 0) {
        const elements = this.isCurrentlyOnWeapons ? this.stats.weaponStats : this.stats.legendStats;
        this.currentElementIndex = elements.length - 1;
      }
    });
    this.addReactionListener(this.nextElementIcon, async () => {
      const elements = this.isCurrentlyOnWeapons ? this.stats.weaponStats : this.stats.legendStats;
      this.currentElementIndex++;
      if (this.currentElementIndex >= elements.length) {
        this.currentElementIndex = 0;
      }
    });

    this.buildLegendStats();
    this.buildWeaponStats();
  }

  public build() {
    this.rebuildSelection();
    return super.build();
  }

  private rebuildSelection(): void {
    if (this.isCurrentlyOnWeapons) {
      this.buildWeaponStats();
    } else {
      this.buildLegendStats();
    }
  }

  private buildWeaponStats(): void {
    const selectedWeapon: IWeaponStats = this.stats.weaponStats[ this.currentElementIndex ];
    const text = `**${selectedWeapon.weapon.name}**\n`+
      `${selectedWeapon.wins} wins / ${selectedWeapon.games} games (${~~(selectedWeapon.wins * 100 / selectedWeapon.games)}%)\n`+
      `**Kos**: ${selectedWeapon.kos}`;
    this.replaceField('Weapon', text);

    const footerText = this.stats.weaponStats.map(weapon=> {
      if(weapon === selectedWeapon){
        return `*${weapon.weapon.name}*`;
      } else {
        return weapon.weapon.name;
      }
    }).join(' | ');
    this.setFooter(footerText);
  }

  private buildLegendStats(): void {
    const selectedLegend: ILegendStats = this.stats.legendStats[ this.currentElementIndex ];
    const overview = `**${selectedLegend.legend.name}**\n` +
      `**Level**: ${selectedLegend.level} (${~~(selectedLegend.xpPercentage * 100)}%)\n`+
      `**Kos**: ${selectedLegend.kos} | **Falls**: ${selectedLegend.falls} | **Suicides**: ${selectedLegend.suicides}\n`+
      `${selectedLegend.wins} wins /${selectedLegend.games} games (${~~(selectedLegend.wins * 100 / selectedLegend.games)}%)\n`;

    const firstWeaponStats = `**${selectedLegend.legend.firstWeapon.name}**: KO: ${selectedLegend.koByFirstWeapon}\n`;
    const secondWeaponStats = `**${selectedLegend.legend.secondWeapon.name}**: KO: ${selectedLegend.koBySecondWeapon}\n`;
    const unarmedStats = `**Unarmed**: KO: ${selectedLegend.koByUnarmed}\n`;

    this.replaceField('Legend', overview + firstWeaponStats + secondWeaponStats + unarmedStats);

    const footerText = this.stats.legendStats.map(legend => {
      if(legend === selectedLegend){
        return `*${legend.legend.name}*`;
      } else {
        return legend.legend.name;
      }
    }).join(' | ');
    this.setFooter(footerText);
  }
}
