import { Command } from './command';
import { ContextType, ModuleType } from '../bot';
import { Message } from 'discord.js';

export class Ping implements Command {
  public readonly name = 'Ping';
  public readonly alias = ['ping'];
  public description = 'This is a ping command, to ensure the bot is working correctly.';
  public arguments = [];
  public usage = 'ping';

  public init(){}

  public execute(y: ContextType, message: Message, ...args: any[]){
    return 'Pong';
  }
}

export const PingInstance: Command = new Ping();
