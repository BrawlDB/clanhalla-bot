import { ContextType, ModuleType } from '../bot';
import { Message, RichEmbed } from 'discord.js';
import { ReactiveRichEmbed } from '../utils/ReactiveEmbed';

export interface Command {
  name: string;
  alias: string[];
  description: string;
  usage: string;
  arguments: CommandArgument[];
  execute: (context: ContextType, message: Message, ...args: any[]) => Promise<CommandResult> | CommandResult;

  init(modules: ModuleType): Promise<void> | void;
}

export interface CommandArgument {
  /*
   * TODO: Add support for those other types
   *  - User
   *  - Role
   *
   * Supported types:
   * string: string
   * number: number
   * mention: The Discord ID of the mentionned user
   * user: The Discord profile of the mentionned user?
   * regex: Must populate the regex field, will return the regex.exec of the value
   * command: Used by the help command, gives the tagged command instance
   * brawlhalla_profile: The BH api result for the tagged user
   */
  type: 'string' | 'number' | 'mention' | 'user' | 'regex' | 'command' | 'brawlhalla_profile';
  regex?: RegExp;
  required?: boolean;
  default?: any;

}

export type CommandResult = string | RichEmbed | ReactiveRichEmbed | Array<string | RichEmbed | ReactiveRichEmbed>;