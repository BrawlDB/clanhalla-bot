import { Command, CommandArgument } from './command';
import { ContextType, ModuleType } from '../bot';
import { Message } from 'discord.js';

export class Prefix implements Command {
  public readonly name = 'Prefix';
  public readonly alias = [ 'prefix' ];
  public description = 'Update the prefix of the bot';
  public arguments: CommandArgument[] = [ { type: 'string' } ];
  public usage = 'prefix [newPrefix]';

  public init(){}

  public async execute({ server }: ContextType, message: Message, newPrefix: string): Promise<string> {
    const oldPrefix = server.prefix;
    await server.setPrefix(newPrefix);
    return `Successfully updated prefix from \`${oldPrefix}\` to \`${newPrefix}\`!`;
  }
}

export const PrefixInstance: Command = new Prefix();
